import matplotlib as mpl
import matplotlib.pyplot as plt
import numpy as np
import torch
from scipy.interpolate import CubicSpline

def calc_courtesy(pred_dict):
    E_reward      = reward(pred_dict['marg_pred_trajectory'],pred_dict['marg_pred_score'])
    E_cond_reward =  reward(pred_dict['cond_pred_trajectory'],pred_dict['cond_pred_score'])
    courtsey = (E_cond_reward-E_reward)/np.abs(E_reward) ## TODO how to deal with negative

    return courtsey,E_reward,E_cond_reward


def reward(pred_traj,pred_traj_score):
    assert np.all(pred_traj_score < 0)
    pred_traj_prob = np.exp(pred_traj_score)
    pred_traj_prob = pred_traj_prob/np.sum(pred_traj_prob)
    E_reward = 0
    for i in range(6):
        plt.plot(pred_traj[i,:,0],pred_traj[i,:,1])
        v_arr,a_arr = fitting(pred_traj[i])
        r = calc_reward(v_arr,a_arr)
        ## calculate progress reward
        E_reward+= pred_traj_prob[i]*r
    
    return E_reward

def fitting(xy_arr):
    x_arr = xy_arr[:,0]
    y_arr = xy_arr[:,1]
    t = range(len(x_arr))
    f_x= CubicSpline(t, x_arr)
    f_y= CubicSpline(t, y_arr)
    f_deriv_x = f_x.derivative()
    f_deriv_y = f_y.derivative()
    vx_arr = f_deriv_x(t)*10
    vy_arr = f_deriv_y(t)*10
    v = np.vstack([vx_arr,vy_arr])
    psi = np.arctan2(vy_arr,vx_arr)
    v_arr = np.linalg.norm(v,axis = 0).squeeze()
    f_v = CubicSpline(t, v_arr)
    f_a = f_v.derivative()
    a_arr = f_a(t)*10
    return v_arr,a_arr


def calc_reward(speed_arr,acc_arr):
    f_v = np.linalg.norm(speed_arr)
    f_a = -np.linalg.norm(acc_arr)
    return f_v #+ f_a
